PROJECT_NAME := stm32h7-test
TARGET_CPU := stm32h7xx

USE_LBOOT := 1

GPG ?= gpg
SIGNED_FILE = $(PROJECT_BIN).sig

OPT := -O2

include ./lib/chip/chip.mk
include ./lib/ltimer/ltimer.mk
include ./lib/lprintf/lprintf.mk
include ./lib/lip/lip.mk


LDSCRIPT_TEMPLATE := STM32H743IITx_FLASH.ld.S

DEFS            := NDEBUG
DEFS += STACK_SIZE=0x1000
DEFS += $(CHIP_DEFS)

# List C source files here
SRC  :=  $(CHIP_SRC)\
         $(CHIP_STARTUP_SRC)\
         $(LTIMER_SRC)\
         $(LPRINTF_SRC) \
         $(LIP_SRC) \
         ./lib/crc/fast_crc.c \
         ./lib/spi_flash/flash.c \
         ./lib/spi_flash/devices/flash_dev_at45d011.c \
         ./lib/spi_flash/ports/stm32h7xx_spi/flash_iface_stm32h7xx_spi.c \
        ./src/main.c



CFLAGS = $(OPT) -std=gnu99
CFLAGS = $(CHIP_CFLAGS)

LDFLAGS  = $(CHIP_LDFLAGS) -nostartfiles


SYS_INC_DIRS := $(CHIP_INC_DIRS)

INC_DIRS := ./src \
            ./lib/lcspec/gcc \
            ./lib/spi_flash \
            ./lib/crc \
           $(LPRINTF_INC_DIRS) \
           $(LUSB_INC_DIRS) \
           $(LIP_INC_DIRS) \
           $(LTIMER_INC_DIRS)


LDDEFS = $(DEFS)

ifdef USE_LBOOT
    DEFS += FLASH_START_OFFSET=0x00020100
    DEFS += USE_LBOOT
    DEFS += LBOOT_USE_TFTP_SERVER LBOOT_USE_HTTP_SERVER
    DEFS += LBOOT_CHIP_FLASH_START_ADDR=0x08000000
    DEFS += LBOOT_CHIP_RAM_START_ADDR=0x20000000

    SRC  += ./lib/crc/crc.c
    SRC  += ./src/lboot.c
else
    DEFS += FLASH_START_OFFSET=0
endif

include ./lib/gnumake_prj/gcc_prj.mk

.PHONY: doc

$(PROJECT_TARGET): $(PROJECT_BIN) $(PROJECT_HEX) $(SIGNED_FILE)


$(SIGNED_FILE): $(PROJECT_BIN)
	$(RMDIR) "$@"
	$(GPG) --detach-sign --digest-algo SHA256 -u Borisov -o "$@" "$<"

