/* Specify the memory areas */
MEMORY
{
DTCMRAM (xrw)     : ORIGIN = 0x20000000, LENGTH = 65536
DTCMRAM_buf (xrw) : ORIGIN = 0x20010000, LENGTH = 65536
RAM_D1 (xrw)      : ORIGIN = 0x24000000, LENGTH = 524288
RAM_D2_RAM1 (xrw) : ORIGIN = 0x30000000, LENGTH = 131072
RAM_D2_RAM2 (xrw) : ORIGIN = 0x30020000, LENGTH = 131072
RAM_D2_RAM3 (xrw) : ORIGIN = 0x30040000, LENGTH = 65536
RAM_D3 (xrw)      : ORIGIN = 0x38000000, LENGTH = 65536
ITCMRAM (xrw)     : ORIGIN = 0x00000000, LENGTH = 64K
FLASH_1 (rx)      : ORIGIN = 0x08000000 + FLASH_START_OFFSET, LENGTH = 1024K - FLASH_START_OFFSET
FLASH_2 (rx)      : ORIGIN = 0x08100000, LENGTH = 1024K
SDRAM (rw)        : ORIGIN = 0xD0000000, LENGTH = 16777216
}

REGION_ALIAS("CODE_ROM", FLASH_1); /* Program code */
REGION_ALIAS("DATA_ROM", FLASH_1); /* Const data and initializers */
REGION_ALIAS("DATA_RAM", DTCMRAM);
REGION_ALIAS("STACK_RAM", DTCMRAM); /* System stack (at the end of the region) */
REGION_ALIAS("HEAP_RAM",  SDRAM); /* System heap (at the end of the region) */

REGION_ALIAS("HUGE_BUFFERS_RAM",  SDRAM); /* memory for very big buffers */

/* Define output sections */
SECTIONS
{
  .text : ALIGN(4) {
    FILL(0xFF)
    . = ALIGN(8);
    _isr_vector_addr = .;
    KEEP(*(.isr_vector .isr_vector.*))
    . = _isr_vector_addr + 0x300;
    KEEP(*(.appl_info))
    . = ALIGN(4);
    *(.text)           /* .text sections (code) */
    *(.text*)          /* .text* sections (code) */
    _etext = .;
  } > CODE_ROM

/* The program code and other data goes into FLASH */
  .text :
  {
    . = ALIGN(4);
  __data_init_tab_start = .;
    LONG(LOADADDR(.data));
    LONG(ADDR (.data));
    LONG(SIZEOF (.data));
  __data_init_tab_end = .;
  __bss_init_tab_start = .;
    LONG(ADDR (.bss));
    LONG(SIZEOF (.bss));
    LONG(ADDR (.bss_oplk));
    LONG(SIZEOF (.bss_oplk));
  __bss_init_tab_end = .;
    . = ALIGN(4);
    *(.rodata)         /* .rodata sections (constants, strings, etc.) */
    *(.rodata*)        /* .rodata* sections (constants, strings, etc.) */
    . = ALIGN(4);
    *(.glue_7)         /* glue arm to thumb code */
    *(.glue_7t)        /* glue thumb to arm code */
    . = ALIGN(4);
    *(.ARM.extab* .gnu.linkonce.armextab.*)
  } >DATA_ROM

  .ARM : {
    . = ALIGN(4);
    __exidx_start = .;
    *(.ARM.exidx* .gnu.linkonce.armexidx.*)
    __exidx_end = .;
  } > DATA_ROM

/* -------- STACK ------------ */
  .stackarea (NOLOAD) : ALIGN(8) {
     *(.stackarea .stackarea.*)
  } > STACK_RAM
/* -------- HEAP --------------*/
  .heaparea (NOLOAD) : ALIGN(8) {
    *(.heaparea .heaparea.*)
  } >HEAP_RAM
/* -------- MAIN RAM -------- */
  .data : ALIGN(4) {
       _data = .;
       *(vtable vtable.*)
       *(.data .data.*)
       *(.gnu.linkonce.d*)
       *(.ram_func)
      . = ALIGN(4);
      _edata = . ;
  } > DATA_RAM AT > DATA_ROM

  .bss_oplk (NOLOAD) : ALIGN(4) {
      _sbss_oplk = .;
      *liboplk*.a:*(.bss* .bss)
      *oplk_*(.bss* .bss)
      *obdcreate*(.bss* .bss)
      _ebss_oplk = .;         /* define a global symbol at bss end */
  } > SDRAM

  .bss (NOLOAD) : ALIGN(4) {
    _bss = . ;
    *(.bss .bss.*)
    *(.gnu.linkonce.b*)
    *(COMMON)
    . = ALIGN(4);
    _ebss = . ;
  } > DATA_RAM

/*----------------------------------------------------------------------------*/


  .INT_TCM_RAM (NOLOAD):
  {
   *(.fast_buffers)
  } >DTCMRAM_buf

  .INT_RAM_D1 (NOLOAD):
  {
    *(.normal_big_buffers)
  } >RAM_D1

  .sdram (NOLOAD):
  {
    *(.sdram)
  } > HUGE_BUFFERS_RAM

  .DMA_Buffers_1 (NOLOAD):
  {
    *(.eth_ram)
  } >RAM_D2_RAM1

  .DMA_Buffers_2 (NOLOAD):
  {
    *(.ADC_Buffers)
    *(.FLASH_Buffers)
  } >RAM_D2_RAM2
}


