#!/bin/sh

openocd -f interface/stlink.cfg -c "adapter_khz 1000; transport select hla_swd" -f target/stm32h7x.cfg  -c "init" -c "reset init" -c "halt" -c "flash write_image erase ./build/stm32h7-test.bin 0x08000000" -c "reset" -c "shutdown"

