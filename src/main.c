#include "chip.h"
#include "ltimer.h"
#include "lprintf.h"
#include "fast_crc.h"
#include "ports/stm32h7xx_spi/flash_iface_stm32h7xx_spi.h"
#include "devices/flash_dev_at45d011.h"
#include "lip.h"
#include "lboot_req.h"
#include "vims_factory_info.h"
#include "vims_extflash_map.h"
#ifdef USE_LBOOT
    #include "lboot.h"
#endif

#define PIN_LED1        CHIP_PIN_PH1_GPIO
#define PIN_LED3        CHIP_PIN_PC2_GPIO
#define PIN_LED4        CHIP_PIN_PC3_GPIO

#define PIN_PHY_RES     CHIP_PIN_PI8_GPIO

#define PIN_BTN         CHIP_PIN_PC0_GPIO



#define TEST_FLASH_ADDR     (7*128*1024)
#define TEST_FLASH_BANK     CHIP_FLASH_BANK1
#define TEST_FLASH_SIZE     (4*20 + 3)
#define TEST_FLASH_WRDS     ((TEST_FLASH_SIZE + 3)/4)




#define TEST_SPI_FLASH_ADDR        (AT45D011_FLASH_SIZE - TEST_SPI_FLASH_SIZE)
#define TEST_SPI_FLASH_SIZE        (264*2)
#define TEST_SPI_FLASH_ERASE_SIZE  (264*2)
#define TEST_SPI_FLASH_WRDS        ((TEST_SPI_FLASH_SIZE + 3)/4)


#define TEST_UDP_PORT       12000

uint8_t mac[LIP_MAC_ADDR_SIZE] = {0x00, 0x05, 0xF7, 0xFF, 0xFF, 0xF1 };
const uint8_t ip[LIP_IPV4_ADDR_SIZE] = {192,168,0,1};
const uint8_t mask[LIP_IPV4_ADDR_SIZE] = {255,255,255,0 };
const uint8_t gate[LIP_IPV4_ADDR_SIZE] = {192,168,0,10};
static int f_btn_pin_val;


/* счетчик, используемый при передачу */
static uint32_t f_snd_cntr = 0;
/* разрешение на передачу */
static int f_snd_en = 0;
/* информация UDP-уровня для передачи */
static t_lip_udp_info f_snd_udp_info;
/* буфер для сохранения адреса удаленного хоста */
static uint8_t f_host_addr[LIP_IP_ADDR_SIZE_MAX];

static t_flash_iface f_ext_flash;

static void f_ext_show_wrds(uint32_t addr) {
    uint32_t wrds[4];
    t_flash_errs err = flash_read_cb(&f_ext_flash, addr, (uint8_t *)wrds, sizeof(wrds), chip_wdt_reset);
    if (err == FLASH_ERR_OK) {
        for (uint32_t i = 0; i < sizeof(wrds)/sizeof(wrds[0]); i++) {
            lprintf("0x%04X: 0x%08X\n", addr + i *4, wrds[i]);
        }
    }
}


/* Обработка принятого UDP-пакета */
static t_lip_errs f_proc_recvd_udp(uint8_t *pkt, size_t size, const t_lip_udp_info *info) {

    if (!strncmp((char*)pkt, "start", 5)) {
        lprintf("start udp test!\n");
        /* копируем адрес удаленного хоста, т.к. info->src_addr действителен
         * только внутри данной функции */
        memcpy(f_host_addr, info->src_addr, LIP_IPV4_ADDR_SIZE);
        /* сохраняем параметры подключения удаленного хоста */
        f_snd_udp_info.ip_type  = info->ip_type;
        f_snd_udp_info.src_port = info->dst_port;
        f_snd_udp_info.dst_port = info->src_port;
        f_snd_udp_info.dst_addr = f_host_addr;

        /* разрешаем передачу */
        f_snd_cntr = 0;
        f_snd_en = 1;
    } else if (!strncmp((char*)pkt, "stop", 4)) {
        lprintf("stop udp test!\n");
        f_snd_en = 0;
    } else if (!strncmp((char*)pkt, "factory-erase", 13)) {
        lprintf("start factory info erase!\n");
        t_flash_errs spiflash_err = flash_erase_cb(&f_ext_flash, VIMS_EXTFLASH_ADDR_FACTORY_INFO, VIMS_FACTORY_INFO_MAX_SIZE, chip_wdt_reset);
        lprintf("factory info erase. res %d\n", spiflash_err);
    } else if (!strncmp((char*)pkt, "set-erase", 9)) {
        lprintf("start set erase!\n");
        t_lclock_ticks ticks = lclock_get_ticks();
        t_flash_errs spiflash_err = flash_erase_cb(&f_ext_flash, VIMS_EXTFLASH_ADDR_SETTINGS, VIMS_EXTFLASH_SETTINGS_REGION_SIZE, chip_wdt_reset);
        lprintf("set erase. res %d, time = %d\n", spiflash_err, lclock_get_ticks() - ticks);
    } else if (!strncmp((char*)pkt, "extfl-wr", 8)) {
        uint32_t rem_size = VIMS_EXTFLASH_SIZE;
        uint32_t wr_addr  = VIMS_EXTFLASH_ADDR_SETTINGS;
        lprintf("start set write!\n");

        static uint32_t wr_buf[VIMS_EXTFLASH_PAGE_SIZE/4];
        uint32_t wr_cnt = 0;
        t_lclock_ticks ticks = lclock_get_ticks();

        t_flash_errs spiflash_err = flash_erase_cb(&f_ext_flash, wr_addr, rem_size, chip_wdt_reset);
        lprintf("erase res %d, time = %d\n", spiflash_err, lclock_get_ticks() - ticks);
        ticks = lclock_get_ticks();

        while ((spiflash_err == FLASH_ERR_OK) && (rem_size > 0)) {
            for (uint32_t i = 0; i < sizeof(wr_buf)/sizeof(wr_buf[0]); i++) {
                wr_buf[i] = wr_cnt++;
            }
            spiflash_err = flash_write_cb(&f_ext_flash, wr_addr, (uint8_t*)wr_buf, sizeof(wr_buf), 0, chip_wdt_reset);
            if (spiflash_err == FLASH_ERR_OK) {
                wr_addr += sizeof(wr_buf);
                rem_size -= sizeof(wr_buf);
            }
        }
        lprintf("write res %d, time = %d\n", spiflash_err, lclock_get_ticks() - ticks);
    } else if (!strncmp((char*)pkt, "extfl-rd", 8)) {
        f_ext_show_wrds(0);
        f_ext_show_wrds(VIMS_EXTFLASH_SETTINGS_REGION_SIZE - 16);
        f_ext_show_wrds(VIMS_EXTFLASH_SETTINGS_REGION_SIZE);
        f_ext_show_wrds(VIMS_EXTFLASH_ADDR_FACTORY_INFO-16);
        f_ext_show_wrds(VIMS_EXTFLASH_ADDR_FACTORY_INFO);
#ifdef USE_LBOOT
    } else if (!strncmp((char*)pkt, "boot-http", 9)) {
        lprintf("boot http request!\n");
        lboot_request(LBOOT_BOOTMODE_HTTP_SERVER, 80);
    } else if (!strncmp((char*)pkt, "boot-tftp", 9)) {
            lprintf("boot tftp request!\n");
            lboot_request(LBOOT_BOOTMODE_TFTP_SERVER, 0);
#endif
    } else {
        lprintf("unknown udp request!\n");
    }
    return LIP_ERR_SUCCESS;
}


int main(void) {
    CHIP_PIN_CONFIG_OUT(PIN_LED1, 1);    
    CHIP_PIN_CONFIG_OUT(PIN_LED3, 1);
    CHIP_PIN_CONFIG_OUT(PIN_LED4, 1);

    CHIP_PIN_CONFIG_IN(PIN_BTN);
    f_btn_pin_val = CHIP_PIN_IN(PIN_BTN);





    CHIP_PIN_OUT(PIN_LED4, 0);

    t_ltimer tmr;
    ltimer_set_ms(&tmr, 10);

    CHIP_PIN_CONFIG_OUT(PIN_PHY_RES, 0);
    while (!ltimer_expired(&tmr)) {}
    CHIP_PIN_OUT(PIN_PHY_RES, 1);



    lprintf_uart_init(115200, 8, LPRINTF_UART_PARITY_NONE);
    //lprintf("123456789012345!\n");

    lprintf("\n\n");
    lprintf("cpu clk:     %d\n", CHIP_CLK_CPU_FREQ);
    lprintf("uart1 clk:   %d\n", CHIP_CLK_USART1_KER_FREQ);
    lprintf("mco1 clk:    %d\n", CHIP_CLK_MCO1_FREQ);
    lprintf("mco2 clk:    %d\n", CHIP_CLK_MCO2_FREQ);
    lprintf("iwdg_rl:     0x%08X\n", ST_IWDG1->RLR);
    lprintf("iwdg_pr:     0x%08X\n", ST_IWDG1->PR);
    lprintf("iwdg_sr:     0x%08X\n", ST_IWDG1->SR);
    lprintf("fl optsr:    0x%08X\n", ST_FLASH->OPTSR_CUR);

    lprintf("bdcr:        0x%08x\n", ST_RCC->BDCR);
    lprintf("pwr_cr2:     0x%08x\n", ST_PWR->CR2);
    lprintf("SYSCFG_UR11: 0x%08x\n", ST_SYSCFG->UR11);

    lprintf("flash acr:   0x%08X\n", ST_FLASH->ACR);
    lprintf("flash wp:    0x%08X\n", chip_flash_get_sectors_wr_protection(TEST_FLASH_BANK));

    lprintf("chip rev:    0x%08X\n", ST_DBGMCU->IDCODE);

    t_lclock_ticks start_ticks;

    chip_wdt_reset();


    chip_wdt_reset();
    start_ticks = lclock_get_ticks();
    chip_delay_clk(CHIP_CLK_CPU_FREQ/10);
    lprintf("delay time = %d, freq = %d\n", lclock_get_ticks() - start_ticks, CHIP_CLK_CPU_FREQ);
    chip_wdt_reset();


    flash_iface_init_stm32h7xx_spi(&f_ext_flash);
    t_flash_errs spiflash_err = flash_set(&f_ext_flash, &flash_info_at45d011);
    lprintf("extflash set err = %d\n", spiflash_err);
    if (spiflash_err == FLASH_ERR_OK) {
        static uint8_t factinfo_buf[VIMS_FACTORY_INFO_MAX_SIZE];
        spiflash_err = flash_read_cb(&f_ext_flash, VIMS_EXTFLASH_ADDR_FACTORY_INFO, factinfo_buf, sizeof(factinfo_buf), chip_wdt_reset);
        if (spiflash_err == FLASH_ERR_OK) {
            int ok = 0;
            t_vims_factory_info *info = (t_vims_factory_info *)factinfo_buf;
            if ((info->sign == VIMS_FACTORY_INFO_SIGN) &&
                     (info->size <= VIMS_FACTORY_INFO_MAX_SIZE)) {
                uint32_t calc_crc, get_crc;
                calc_crc = CRC32_Block8(0, factinfo_buf, info->size - 4);
                memcpy(&get_crc, &factinfo_buf[info->size - 4], 4);
                if (calc_crc == get_crc) {
                    lprintf("lboot: found valid factory info!\n");
                    ok = 1;
                    uint32_t base_size = info->size - 4;
                    if (base_size >= (offsetof(t_vims_factory_info, mac) + sizeof(info->mac))) {
                        memcpy(mac, info->mac, sizeof(info->mac));
                    }
                    if (base_size >= (offsetof(t_vims_factory_info, serial) + sizeof(info->serial))) {
                        lprintf("dev serial: %s\n", info->serial);
                    }
                }
            }


            if (!ok) {
                t_lclock_ticks ticks = lclock_get_ticks();
                lprintf("cannot found factory info. start write new version!\n");
                spiflash_err = flash_erase_cb(&f_ext_flash, VIMS_EXTFLASH_ADDR_FACTORY_INFO, VIMS_FACTORY_INFO_MAX_SIZE, chip_wdt_reset);
                if (spiflash_err == FLASH_ERR_OK) {
                    info->sign = VIMS_FACTORY_INFO_SIGN;
                    info->size = sizeof(t_vims_factory_info) + 4;
                    memcpy(info->mac, mac, sizeof(info->mac));
                    strcpy((char*)info->serial, "1R12345");
                    uint32_t crc = CRC32_Block8(0, factinfo_buf, info->size - 4);
                    memcpy(&factinfo_buf[info->size - 4], &crc, 4);
                    spiflash_err = flash_write_cb(&f_ext_flash, VIMS_EXTFLASH_ADDR_FACTORY_INFO,
                                                   factinfo_buf, info->size, 0, chip_wdt_reset);
                    lprintf("factory info wr. err = %d, size = %d, time = %d\n",
                            spiflash_err, info->size, lclock_get_ticks() - ticks);
                }
            }
        }
    }



    if (spiflash_err == FLASH_ERR_OK) {

    }








#if 0
    uint8_t status[3];
    for (int i = 0; i < 10; i++) {
        uint8_t cmd = 0x57;
        flash_exec_cmd(&flash, &cmd, 1, NULL,0, status,3, FLASH_FLAGS_FLUSH);

        ///flash_get_status(&flash, &status);
        lprintf("flash status = 0x%02X, 0x%02X, 0x%02X\n", status[0], status[1], status[2]);
    }
#endif
#if 0
    if (spiflash_err == FLASH_ERR_OK) {
        static uint32_t flash_buf[TEST_SPI_FLASH_WRDS];
        start_ticks = lclock_get_ticks();       
        spiflash_err = flash_read(&flash, TEST_SPI_FLASH_ADDR, (uint8_t*)&flash_buf, sizeof(flash_buf));
        lprintf("spi flash read done %d, %d ms\n", spiflash_err, lclock_get_ticks() - start_ticks);
        if (!spiflash_err) {
            for (int i = 0; i < TEST_SPI_FLASH_WRDS; i++) {
                lprintf("%3d: 0x%08X\n", i, flash_buf[i]);
                chip_wdt_reset();
            }


            start_ticks = lclock_get_ticks();
            spiflash_err = flash_erase(&flash, TEST_SPI_FLASH_ADDR, TEST_SPI_FLASH_ERASE_SIZE);
            lprintf("spi flash erase done %d, %d ms\n", spiflash_err, lclock_get_ticks() - start_ticks);
        }

        if (!spiflash_err) {
            spiflash_err = flash_read(&flash, TEST_SPI_FLASH_ADDR, (uint8_t*)&flash_buf, sizeof(flash_buf));
            for (int i = 0; i < TEST_SPI_FLASH_WRDS; i++) {
                lprintf("%3d: 0x%08X\n", i, flash_buf[i]);
                chip_wdt_reset();
            }
            uint32_t *wr_buf = (uint32_t *)flash_buf;
            for (uint32_t i = 0; i < TEST_SPI_FLASH_WRDS; i++) {
                wr_buf[i] = 1000 + i;
            }
            start_ticks = lclock_get_ticks();
            spiflash_err = flash_write(&flash, TEST_SPI_FLASH_ADDR, (uint8_t*)wr_buf, TEST_SPI_FLASH_SIZE, 0);
            lprintf("spi flash write done %d, %d ms\n", spiflash_err, lclock_get_ticks() - start_ticks);
        }

        if (!spiflash_err) {
            spiflash_err = flash_read(&flash, TEST_SPI_FLASH_ADDR, (uint8_t*)&flash_buf, sizeof(flash_buf));
            for (int i = 0; i < TEST_SPI_FLASH_WRDS; i++) {
                lprintf("%3d: 0x%08X\n", i, flash_buf[i]);
                chip_wdt_reset();
            }
        }

        if (!spiflash_err) {
            uint8_t wr_test[3] = {0xAB, 0xCD, 0xEF};
            start_ticks = lclock_get_ticks();
            spiflash_err = flash_write(&flash, TEST_SPI_FLASH_ADDR + TEST_FLASH_SIZE - 16, (uint8_t*)wr_test, sizeof(wr_test), 0);
            lprintf("spi flash write2 done %d, %d ms\n", spiflash_err, lclock_get_ticks() - start_ticks);
        }

        if (!spiflash_err) {
            spiflash_err = flash_read(&flash, TEST_SPI_FLASH_ADDR, (uint8_t*)&flash_buf, sizeof(flash_buf));
            for (int i = 0; i < TEST_SPI_FLASH_WRDS; i++) {
                lprintf("%3d: 0x%08X\n", i, flash_buf[i]);
                chip_wdt_reset();
            }
        }
    }
#endif


#if 0

    t_chip_flash_err fl_err;

    start_ticks = lclock_get_ticks();
    fl_err = chip_flash_sectors_wr_protect(TEST_FLASH_BANK, (1 << 5) | (1 << 4),  chip_wdt_reset);
    fl_err = chip_flash_wr_protect(TEST_FLASH_BANK, TEST_FLASH_ADDR, TEST_FLASH_SIZE, chip_wdt_reset);
    lprintf("flash wp set. res %d. time %d, reg 0x%08X\n", fl_err, lclock_get_ticks() - start_ticks, chip_flash_get_sectors_wr_protection(TEST_FLASH_BANK));

    const uint32_t *flash_buf = (const uint32_t *)chip_flash_rd_addr(TEST_FLASH_BANK, TEST_FLASH_ADDR);
    lprintf("flash data:\n");
    for (int i = 0; i < TEST_FLASH_WRDS; i++) {
        lprintf("%3d: 0x%08X\n", i, flash_buf[i]);
    }


    start_ticks = lclock_get_ticks();
    chip_flash_wr_unprotect(TEST_FLASH_BANK, TEST_FLASH_ADDR, TEST_FLASH_SIZE, chip_wdt_reset);
    lprintf("flash wp unset. res %d. time %d, reg 0x%08X\n", fl_err, lclock_get_ticks() - start_ticks, chip_flash_get_sectors_wr_protection(TEST_FLASH_BANK));

    start_ticks = lclock_get_ticks();
    chip_flash_unlock(TEST_FLASH_BANK);


    fl_err = chip_flash_erase(TEST_FLASH_BANK, TEST_FLASH_ADDR, TEST_FLASH_SIZE, chip_wdt_reset);
    lprintf("erase done. res = %d, time %d\n", fl_err, lclock_get_ticks() - start_ticks);

    lprintf("flash data:\n");
    for (int i = 0; i < TEST_FLASH_WRDS; i++) {
        lprintf("%3d: 0x%08X\n", i, flash_buf[i]);
    }

    uint8_t test_buf[TEST_FLASH_SIZE];
    for (uint8_t i = 0; i < TEST_FLASH_SIZE; i++)
        test_buf[i] = i;

    start_ticks = lclock_get_ticks();
    fl_err = chip_flash_write(TEST_FLASH_BANK, TEST_FLASH_ADDR, test_buf, sizeof(test_buf), chip_wdt_reset);
    lprintf("write done. res = %d, time %d\n", fl_err, lclock_get_ticks() - start_ticks);

    lprintf("flash data:\n");
    for (int i = 0; i < TEST_FLASH_WRDS + 2; i++) {
        lprintf("%3d: 0x%08X\n", i, flash_buf[i]);
    }

    start_ticks = lclock_get_ticks();
    fl_err = chip_flash_wr_protect(TEST_FLASH_BANK, TEST_FLASH_ADDR, TEST_FLASH_SIZE, chip_wdt_reset);
    lprintf("flash wp set. res %d. time %d, reg 0x%08X\n", fl_err, lclock_get_ticks() - start_ticks, chip_flash_get_sectors_wr_protection(TEST_FLASH_BANK));

#endif

    t_lip_errs liperr = lip_init();
    lprintf("lip init = %d\n", liperr);
    /* инициализация стека */
    lip_init();

    /* MAC-адрес всегда устанавливается сразу после lip_init().
       Для изменения нужно вызвать lip_close(), lip_init() и снова
        lip_eth_set_mac_addr()*/
    lip_eth_set_mac_addr(mac);
    lip_ipv4_set_addr(ip);
    lip_ipv4_set_mask(mask);
    lip_ipv4_set_gate(gate);

    /* Обнуляем информацию о удаленном хосте для передачи. Она будет
        проинициализирована при приходе пакета со строкой "start" от удаленного
        хоста */
    memset(&f_snd_udp_info, 0, sizeof(f_snd_udp_info));

    /* устанавливаем функцию, которая будет вызываться при приходе UDP пакета
       на заданный порт */
    lip_udp_port_listen(TEST_UDP_PORT, f_proc_recvd_udp);

    ltimer_set_ms(&tmr, 1000);
    for (;;) {
        int btn = CHIP_PIN_IN(PIN_BTN);
        if (btn != f_btn_pin_val) {
            lprintf("change button state: %d\n", btn);
            f_btn_pin_val = btn;
        }

        lip_pull();
#ifdef USE_LBOOT
        lboot_pull();
#endif
        chip_wdt_reset();

        if (ltimer_expired(&tmr)) {
            ltimer_reset(&tmr);
            CHIP_PIN_TOGGLE(PIN_LED4);
        }

        /* Если разрешена передача, то при наличии линка и свободного буфера на
           передачу, формируем пакет с счетчиком и отправляем */
        if (f_snd_en && (lip_state()==LIP_STATE_CONFIGURED) && lip_tx_buf_rdy()) {
            int max_size = 0;
            /* подготовка UDP пакета для передачи, получаем указатель на буфер для
               записи данных и макс. размер данных в max_size */
            uint32_t *put_buf = (uint32_t*)lip_udp_prepare(&f_snd_udp_info, &max_size);
            if (put_buf!=NULL) {
                int snd_size = 0;
                for (int i=0; i < max_size/4; i++) {
                    put_buf[i] = f_snd_cntr++;
                    snd_size+=4;
                }
                /* передача сформированного пакета */
                lip_udp_flush(snd_size);
            }
        }
    }
}

void close(void) {
    lip_close();
    lprintf_close();
    CHIP_PIN_CONFIG_IN(PIN_LED1);
    CHIP_PIN_CONFIG_IN(PIN_LED3);
    CHIP_PIN_CONFIG_IN(PIN_LED4);
}
