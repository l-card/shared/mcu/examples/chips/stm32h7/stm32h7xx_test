#ifndef VIMS_FACTORY_INFO_H
#define VIMS_FACTORY_INFO_H


#define VIMS_FACTORY_INFO_SIGN      0x56494D46
#define VIMS_FACTORY_INFO_MAX_SIZE  264
#define VIMS_FACTORY_INFO_MIN_SIZE  50

/* общая часть начала заводской информации о модуле */
typedef struct {
    uint32_t sign; /* сигнатура, должна всегда быть равна VIMS_FACTORY_INFO_SIGN */
    uint32_t size; /* размер всей информации, включая завершающий CRC32 */
    uint8_t serial[32]; /* серийный номер модуля */
    uint8_t mac[6]; /* MAC-адрес модуля */
} t_vims_factory_info;

#endif // VIMS_FACTORY_INFO_H

