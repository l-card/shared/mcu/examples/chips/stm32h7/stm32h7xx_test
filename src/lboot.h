#ifndef LBOOT_H
#define LBOOT_H

#include <stdint.h>
#include "lboot_req.h"
void lboot_start_bootloader(void);
void lboot_pull(void);
void lboot_request(int mode, uint16_t port);

#endif // LBOOT_H
