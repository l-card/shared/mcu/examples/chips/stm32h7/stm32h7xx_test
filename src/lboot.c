#include "lboot.h"
#include "chip.h"
#include "lboot_req.h"
#include "lip.h"
#include "crc.h"
#include "ltimer.h"

#define FW_VERSION  "0.1"

extern void close(void) ;
static t_lboot_params f_boot_params;
static t_ltimer f_boot_tmr;
static int f_boot_req;

__attribute__ ((section(".appl_info")))
const t_lboot_app_info g_app_info = { sizeof(t_lboot_app_info),
        LBOOT_APP_FLAGS_STABLE,       //flags
        "VIMS_ICP",  //device name
        FW_VERSION
        };

static const t_lboot_devinfo devinfo = {
    "VIMS_ICP",
    "",
    FW_VERSION,
    "",
    "",
    ""
};


void lboot_start_bootloader(void) {
    lprintf("start boot!\n");
    __disable_irq();

     //устанавливаем стек и
    asm volatile (
       "movs r2, #4 \n\t"
       "movs r3, #1 \n\t"
       "skip_cfg: \n\t"
       "LDR  r1, [%1, #0] \n\t"
       "STR  r1, [%0, #0] \n\t"
       "ADD  %0, r2 \n\t"
       "ADD  %1, r2 \n\t"
       "SUBS %2, r3 \n\t"
       "CMP  %2, #0 \n\t"
       "BNE skip_cfg \n\t"
       "movs r2, %3 \n\t"
       "ldr r1, [r2] \n\t"
       "msr MSP, r1 \n\t"
       "ldr r1, [r2,#4] \n\t"
       "bx  r1 \n\t"
       : : "l" (LBOOT_REQ_ADDR), "l" (&f_boot_params), "l" ((f_boot_params.hdr.size+3)/4), "l" (LBOOT_CHIP_FLASH_START_ADDR)
       : "r1", "r2", "r3" );
}

void lboot_pull(void) {
    if (f_boot_req && ltimer_expired(&f_boot_tmr)) {
        close();
        lboot_start_bootloader();
    }
}

static void set_boot_req(void) {
    f_boot_req = 1;
    ltimer_set_ms(&f_boot_tmr, 500);
}



void lboot_request(int mode, uint16_t port) {
     f_boot_params.hdr.flags = LBOOT_REQ_FLAGS_RECOVERY_WR;
     f_boot_params.hdr.devinfo = devinfo;
     f_boot_params.hdr.timeout = 90000;
    if (mode == LBOOT_BOOTMODE_TFTP_SERVER) {
        if (lip_ipv4_cur_addr_is_valid()) {
            f_boot_params.hdr.size = LBOOT_REQ_SIZE(tftp);
            f_boot_params.hdr.bootmode = LBOOT_BOOTMODE_TFTP_SERVER;
            memset(&f_boot_params.tftp, 0, sizeof(t_lboot_specpar_tftp));
            memcpy(f_boot_params.tftp.mac, lip_eth_cur_mac(), LIP_MAC_ADDR_SIZE);
            lip_ipv4_addr_cpy(f_boot_params.tftp.l_ip, lip_ipv4_cur_addr());
            lip_ipv4_addr_cpy(f_boot_params.tftp.mask, lip_ipv4_cur_mask());
            lip_ipv4_addr_cpy(f_boot_params.tftp.gate, lip_ipv4_cur_gate());
            f_boot_params.tftp.crc = eval_crc16(0, (uint8_t*)&f_boot_params, f_boot_params.hdr.size - sizeof(f_boot_params.tftp.crc));
            set_boot_req();
        }
    } else if (mode == LBOOT_BOOTMODE_HTTP_SERVER) {
        if (lip_ipv4_cur_addr_is_valid()) {

            f_boot_params.hdr.size = LBOOT_REQ_SIZE(http);
            f_boot_params.hdr.bootmode = LBOOT_BOOTMODE_HTTP_SERVER;
            memset(&f_boot_params.http, 0, sizeof(t_lboot_specpar_tftp));
            memcpy(f_boot_params.http.mac, lip_eth_cur_mac(), LIP_MAC_ADDR_SIZE);
            lip_ipv4_addr_cpy(f_boot_params.http.l_ip, lip_ipv4_cur_addr());
            lip_ipv4_addr_cpy(f_boot_params.http.mask, lip_ipv4_cur_mask());
            lip_ipv4_addr_cpy(f_boot_params.http.gate, lip_ipv4_cur_gate());
            f_boot_params.http.server_port = port;
            f_boot_params.http.crc = eval_crc16(0, (uint8_t*)&f_boot_params, f_boot_params.hdr.size - sizeof(f_boot_params.tftp.crc));
            set_boot_req();

        }
    }
}
