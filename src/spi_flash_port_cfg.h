#ifndef SPI_FLASH_PORT_CFG_H
#define SPI_FLASH_PORT_CFG_H

#include "chip.h"

/* номер SPI интерфейса */
#define FLASH_SPI_IFACE_NUM  3

/* назначение пинов */
#define FLASH_SPI_PIN_SCK    CHIP_PIN_PC10_SPI3_SCK
#define FLASH_SPI_PIN_MOSI   CHIP_PIN_PC12_SPI3_MOSI
#define FLASH_SPI_PIN_MISO   CHIP_PIN_PC11_SPI3_MISO
#define FLASH_SPI_PIN_CS     CHIP_PIN_PD6_GPIO

/* максимальная частота клока SPI */
#define FLASH_SPI_SCK_FREQ_MAX          CHIP_MHZ(15)
/* время удержания CS до первого клока в нс */
#define FLASH_SPI_CS_SETUP_TIME_NS_MIN  2250
/* режим работы SPI */
#define FLASH_SPI_MODE_CPOL         1
#define FLASH_SPI_MODE_CPHA         1



#endif // SPI_FLASH_PORT_CFG_H
