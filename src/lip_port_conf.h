#ifndef LIP_PORT_CONFIG_H
#define LIP_PORT_CONFIG_H

/** Макрос для задания расположения буферов приема и передачи пакетов */
#define LIP_PORT_BUF_MEM(var)    __attribute__ ((section (".eth_ram"))) var
/** Макрос для задания расположения дескрипторов DMA для Ethernet*/
#define LIP_PORT_DESCR_MEM(var) __attribute__ ((section (".eth_ram"))) var

#endif // LIP_PORT_CONFIG_H
