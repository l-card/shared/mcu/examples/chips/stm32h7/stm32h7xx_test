﻿#ifndef CHIP_CONFIG_H
#define CHIP_CONFIG_H

/* включение опций, поддержанных в ревизии RevV чипа */
#define CHIP_REV_V_EN            1

/* ------------------ Настройки питания --------------------------------------*/
#define CHIP_PWR_LDO_EN         1 /* использование LDO для питания ядра (по умолчанию) */
#define CHIP_PWR_BYPASS_EN      0 /* режим без использования power manager */


#define CHIP_PWR_VOS            CHIP_PWR_VOS1 /* управление напряжением ядра: VOS1, VOS2 или VOS3
                                                 влияет на потеребление и макс. частоту с выхода
                                                 PLL (VOS1 - наибольшая производительность) */

#define CHIP_PWR_BACKUP_REG_EN      1 /* Разрешения backup regulator для питания от батареи.
                                         Для применения настройки должен быть разрешен CHIP_PWR_BACKUP_REG_CFG_EN */
#define CHIP_PWR_BACKUP_REG_CFG_EN  1 /* Разрешение изменения конфигурации CHIP_PWR_BACKUP_REG_EN */


#define CHIP_PWR_MON_EN              0 /* Разрешение мониторинга Vbat и температуры.
                                          Для применения настройки должен быть разрешен CHIP_PWR_MON_CFG_EN */
#define CHIP_PWR_MON_CFG_EN          1 /* Разрешение изменения настройки CHIP_PWR_MON_EN */



/* ------------------ Внешний высокочастотный источник частоты HSE -----------*/
#define CHIP_CLK_HSE_FREQ       CHIP_MHZ(24)              /* значение внешней частоты HSE */
#define CHIP_CLK_HSE_MODE       CHIP_CLK_EXTMODE_CLOCK    /* режим внешнего источника HSE - внешний клок */
#define CHIP_CLK_HSE_CSS_EN     1                         /* разрешение контроля внешней частоты HSE */


/* ------------------ Внешний низкочастотный источник частоты LSE ------------*/
#define CHIP_CLK_LSE_CFG_EN     1                          /* Разрешение изменения конфигурации LSE. */
#define CHIP_CLK_LSE_MODE       CHIP_CLK_EXTMODE_OSC       /* Режим внешнего источника LSE - осциллятор.
                                                              для осциллятора можно не указывать частоты, т.к. фиксированная */
#define CHIP_CLK_LSE_DRV        CHIP_CLK_LSE_DRV_LOWEST    /* Сила драйвера LSE осциллятора (LOWEST, LOW, HIGH, HIGHEST) */
#define CHIP_CLK_LSE_CSS_EN     1                          /* Разрешение контроля внешней частоты LSE */

#define CHIP_CLK_LSE_RDY_TOUT_MS 100                       /* время ожидания готовности LSE. Если < 0 - бесконечное */



/* ------------------ Внутренний высокочастотный источник частоты HSI --------*/
#define CHIP_CLK_HSI_EN         1           /* разрешение источника частоты HSI */
#define CHIP_CLK_HSI_FREQ       CHIP_MHZ(64) /* частота источника HSI (из набора 8, 16, 32 или 64 МГц) */
#define CHIP_CLK_HSI_ON_STOP_EN 0           /* разрешение источника частоты HSI в режиме STOP */


/* ------------------ Внутренний низкочастотный источник частоты LSI ---------*/
#define CHIP_CLK_LSI_EN  1  /* разрешение источника частоты LSI */

/* ------------------ Внутренний низкопотребляющий источник частоты CSI ------*/
#define CHIP_CLK_CSI_EN          0        /* разрешение источника частоты CSI */
#define CHIP_CLK_CSI_ON_STOP_EN  0        /* разрешение источника частоты CSI в режиме STOP */

/*  ----------------- Внетренний источник частоты 48 МГц ---------------------*/
#define CHIP_CLK_HSI48_EN        0

/* ------------------ Внешняя частота, подаваемая на пин I2S_CKIN ------------*/
#define CHIP_CLK_I2S_CKIN_EN     0
#define CHIP_CLK_I2S_CKIN_FREQ   CHIP_MHZ(60)

/* ---------------------- Настройки PLL ---------------------------------------*/
/*******************************************************************************
 * Может быть настроено до 3-х PLL, каждая из которых имеет до 3-х выходов (P,R,Q).
 * Источник частоты для всех PLL один и задается CHIP_CLK_PLL_SRC.
 * Для каждой PLL настраивается независимо:
 *  1. Разрешение PLL и всех ее настроек (EN)
 *  2. Делитель входной частоты DIVM от 1 до 63 (результирующая частота fin должна быть от 1 до 16МГц)
 *  3. Множитель DIVN (4..512) и его дробная часть (FRACN) для получения частоты
 *      fvco = fin * (divn + fracn/2^13)
 *     fvco должна быть в диапазоне:
 *       192-836 МГц для fin > 2 МГц
 *       150-420 МГц для fin <= 2 МГц
 *  4. Для каждого выхода настраивается его разрешение (DIVP_EN, DIVR_EN, DIVQ_EN)
 *      и собственно значение делителя (DIVP, DIVR, DIVQ) от 1 до 128
 *      (PLL1_DIVP может быть только четным)
 ******************************************************************************/
#define CHIP_CLK_PLL_SRC      CHIP_CLK_HSE  /* источник частоты (HSI, CSI, HSE) для
                                               всех 3-х PLL.
                                               Если все PLL запрещены, то не используется */
#define CHIP_CLK_PLL1_EN       1
#define CHIP_CLK_PLL1_DIVM     6   /* делитель входной частоты PLL1 (результирующая частота должна быть 1-16 МГц */
#define CHIP_CLK_PLL1_DIVN     200 /* множитель, для получения частоты fvco = fin * (divn + fracn/2^13) */
#define CHIP_CLK_PLL1_FRACN    0   /* дробная часть множителя */
#define CHIP_CLK_PLL1_DIVP_EN  1 /* разреение выхода P */
#define CHIP_CLK_PLL1_DIVP     2 /* частота 400 МГц для процессора */
#define CHIP_CLK_PLL1_DIVR_EN  0
#define CHIP_CLK_PLL1_DIVQ_EN  1
#define CHIP_CLK_PLL1_DIVQ     8 /* частота 100 МГц */

/* на PLL2 получаем частоту для UART - 221184 КHz, чтобы можно было точно выставить вплоть до 921600 */
#define CHIP_CLK_PLL2_EN      1
#define CHIP_CLK_PLL2_DIVM    24
#define CHIP_CLK_PLL2_DIVN    221
#define CHIP_CLK_PLL2_FRACN   1507
#define CHIP_CLK_PLL2_DIVP_EN  0
#define CHIP_CLK_PLL2_DIVR_EN  0
#define CHIP_CLK_PLL2_DIVQ_EN  1
#define CHIP_CLK_PLL2_DIVQ     3 /* выходная частота для UART 73728 KHz  */

#define CHIP_CLK_PLL3_EN      0


/* --------------------- Системный клок --------------------------------------*/
#define CHIP_CLK_SYS_SRC         CHIP_CLK_PLL1_P /* источник частоты для системного клока: HSI, CSI, HSE или PLL1_P */

/* --------------------- Частоты CPU, шин и таймеров -------------------------*/
#define CHIP_CLK_D1_CPU_DIV      1      /* делитель входной системной частоты (SYS) домена D1 для
                                           получения частоты CPU из набора 1,2,4,8,16,64,128,256,512. */

#define CHIP_CLK_HOST_DIV        2      /* дополнительный делитель частоты CPU для получения частот AHB/AXI из набора 1,2,4,8,16,64,128,256,512 */
#define CHIP_CLK_APB1_DIV        2      /* дополнительный делитель частоты AHB (HOST) для получения частоты APB1 в D2 из набора 1,2,4,8,16 */
#define CHIP_CLK_APB2_DIV        2      /* дополнительный делитель частоты AHB (HOST) для получения частоты APB2 в D2 из набора 1,2,4,8,16 */
#define CHIP_CLK_APB3_DIV        2      /* дополнительный делитель частоты AHB (HOST) для получения частоты APB3 в D1 из набора 1,2,4,8,16 */
#define CHIP_CLK_APB4_DIV        2      /* дополнительный делитель частоты AHB (HOST) для получения частоты APB4 в D3 из набора 1,2,4,8,16 */


#define CHIP_CLK_TIMX_DIV        1      /* делитель частоты таймеров X относительно AHB1 (1, 2, 4, 8). Возможные значения зависят от CHIP_CLK_APB1_DIV */
#define CHIP_CLK_TIMY_DIV        1      /* делитель частоты таймеров Y относительно AHB2 (1, 2, 4, 8). Возможные значения зависят от CHIP_CLK_APB2_DIV */
#define CHIP_CLK_HRTIM_KER_SRC   CHIP_CLK_TIMY_KER /* источник клока для High Resolution Timer - TIMY_KER или CPU */


/* ------------------------- RTC ---------------------------------------------*/
#define CHIP_CLK_RTC_EN          1 /* разрешение клока RTC */
#define CHIP_CLK_RTC_CFG_EN      1 /* разрешение изменения конфигурации клока RTC. */
#define CHIP_CLK_RTC_SRC         CHIP_CLK_LSE /* источник частоты для RTC (LSE, LSI, RTC_HSE).
                                                     Если RTC_HSE, то должен быть задан делитель CHIP_CLK_RTC_HSE_DIV */
#define CHIP_CLK_RTC_HSE_DIV     2 /* делитель клока HSE при подаче на RTC (2-63) */

/* ------------------ Источники частот при пробуждении -----------------------*/
#define CHIP_CLK_WU_SRC         CHIP_CLK_HSI  /* источник клока при пробуждении (wakeup): HSI или CSI */
#define CHIP_CLK_WU_KER_SRC     CHIP_CLK_HSI  /* источник kernel-клока при пробуждении (wakeup): HSI или CSI */

/* ----------------------- Настройки MCO (выходы частоты) --------------------*/
#define CHIP_CLK_MCO1_EN         1 /* разрешение генерации выходной частоты MCO1 */
#define CHIP_CLK_MCO1_SRC        CHIP_CLK_PLL1_Q /* Источник частоты для MCO1 (HSI, LSE, HSE, PLL1_Q, HSI48) */
#define CHIP_CLK_MCO1_DIV        4 /* делитель частоты от 1 до 15 */

#define CHIP_CLK_MCO2_EN         0 /* разрешение генерации выходной частоты MCO1 */
#define CHIP_CLK_MCO2_SRC        CHIP_CLK_SYS /* Источник частоты для MCO1 (SYS, PLL2_P, HSE, PLL1_P, CSI, LSI) */
#define CHIP_CLK_MCO2_DIV        1 /* делитель частоты от 1 до 15 */


/* -------------------------- Периферия ------------------------------------- */
#define CHIP_CLK_PER_SRC                CHIP_CLK_HSI_KER /* Источник частоты клока per_ck (HSI_KER, CSI_KER, HSE).
                                                            Промежуточная частота, которая может использоваться
                                                            как источник ker_ck для некоторой периферии */

/*******************************************************************************
 * Настройки источников частот для kernel clock (ker_ck) периферии, для которой
 * данный клок может независимо настраиваться. Одна настройка может определять
 * источник ker_ck сразу для нескольких однотипных блоков.
 *
 * Настройка источника соответствующего ker_ck с проверкой диапазона и разрешения
 * выбранной частоты происходит, если выполняется одно из условий:
 *   - разрешен клок хотя бы одного блока, для которого настройка определяет источник ker_ck.
 *      Например для CHIP_CLK_USART16_KER_SRC включение конфигурации произойдет,
 *       если разрешена одна из настроек CHIP_CLK_UART1_EN или CHIP_CLK_UART6_EN.
 *   - явно разрешена конфигурация источника частоты через определение значения
 *      ***_KER_SRC_CFG_EN, отличного от нуля. При одной настройке для нескольких
 *      блоков в качестве префикса может использоваться как общее название,
 *      т.к. и отдельного блока. Например для CHIP_CLK_USART16_KER_SRC включение
 *      конфигурации произойдет при определении одного из следующих параметров:
 *      CHIP_CLK_USART16_KER_SRC_CFG_EN, CHIP_CLK_USART1_KER_SRC_CFG_EN
 *      или CHIP_CLK_USART6_KER_SRC_CFG_EN. Данная возможность используется,
 *      если клок блока не требуется разрешать при старте, но будет разрешен
 *      во время работы с помощью chip_per_clk_en()/chip_per_clk_dis(), в результате
 *      чего при старте требуется только настройка источника частоты
 * Если ни одно из этих условий не выполненно, то соответствующий параметр не
 *      будет использоваться и может быть не определен
 ******************************************************************************/
#define CHIP_CLK_FMC_KER_SRC            CHIP_CLK_AHB3   /* AHB3, PLL1_Q, PLL2_R, PER */

#define CHIP_CLK_LPTIM1_KER_SRC         CHIP_CLK_APB1   /* APB1, PLL2_P, PLL3_R, LSE, LSI, PER */
#define CHIP_CLK_LPTIM2_KER_SRC         CHIP_CLK_APB4   /* APB4, PLL2_P, PLL3_R, LSE, LSI, PER */
#define CHIP_CLK_LPTIM345_KER_SRC       CHIP_CLK_APB4   /* APB4, PLL2_P, PLL3_R, LSE, LSI, PER */

#define CHIP_CLK_USART16_KER_SRC        CHIP_CLK_PLL2_Q /* APB2, PLL2_Q, PLL3_Q, HSI_KER, CSI_KER, LSE */
#define CHIP_CLK_USART234578_KER_SRC    CHIP_CLK_APB1   /* APB1, PLL2_Q, PLL3_Q, HSI_KER, CSI_KER, LSE */
#define CHIP_CLK_LPUART1_KER_SRC        CHIP_CLK_APB4   /* APB4, PLL2_Q, PLL3_Q, HSI_KER, CSI_KER, LSE */

#define CHIP_CLK_QSPI_KER_SRC           CHIP_CLK_AHB3   /* AHB3, PLL1_Q, PLL2_R, PER */
#define CHIP_CLK_SPI123_KER_SRC         CHIP_CLK_PLL1_Q /* PLL1_Q, PLL2_P, PLL3_P, I2S_CKIN, PER */
#define CHIP_CLK_SPI45_KER_SRC          CHIP_CLK_PLL2_Q /* APB2, PLL2_Q, PLL3_Q, HSI_KER, CSI_KER, HSE */
#define CHIP_CLK_SPI6_KER_SRC           CHIP_CLK_APB4   /* APB4, PLL2_Q, PLL3_Q, HSI_KER, CSI_KER, HSE */

#define CHIP_CLK_I2C123_KER_SRC         CHIP_CLK_APB1   /* APB1, PLL3_R, HSI_KER, CSI_KER */
#define CHIP_CLK_I2C4_KER_SRC           CHIP_CLK_APB4   /* APB4, PLL3_R, HSI_KER, CSI_KER */

#define CHIP_CLK_SDMMC_KER_SRC          CHIP_CLK_PLL1_Q /* PLL1_Q или PLL2_R */
#define CHIP_CLK_USB_KER_SRC            CHIP_CLK_PLL1_Q /* PLL1_Q, PLL3_Q, HSI48 */
#define CHIP_CLK_ADC_KER_SRC            CHIP_CLK_PER    /* PLL2_P, PLL3_R, PER */

#define CHIP_CLK_SAI1_KER_SRC           CHIP_CLK_PLL1_Q /* PLL1_Q, PLL2_P, PLL3_P, I2S_CKIN, PER */
#define CHIP_CLK_SAI23_KER_SRC          CHIP_CLK_PLL1_Q /* PLL1_Q, PLL2_P, PLL3_P, I2S_CKIN, PER */
#define CHIP_CLK_SAI4A_KER_SRC          CHIP_CLK_PER    /* PLL1_Q, PLL2_P, PLL3_P, I2S_CKIN, PER */
#define CHIP_CLK_SAI4B_KER_SRC          CHIP_CLK_PER    /* PLL1_Q, PLL2_P, PLL3_P, I2S_CKIN, PER */
#define CHIP_CLK_SPDIF_KER_SRC          CHIP_CLK_PLL1_Q /* PLL1_Q, PLL2_R, PLL3_R, HSI_KER */
#define CHIP_CLK_DFSDM1_KER_SRC         CHIP_CLK_APB2    /* APB2, SYS */

#define CHIP_CLK_FDCAN_KER_SRC          CHIP_CLK_HSE     /* HSE, PLL1_Q, PLL2_Q */
#define CHIP_CLK_SWP_KER_SRC            CHIP_CLK_HSI_KER /* APB1, HSI_KER */
#define CHIP_CLK_RNG_KER_SRC            CHIP_CLK_LSE     /* HSI48, PLL1_Q, LSE, LSI */

/* Опциональные параметры для принудительного разрешения настройки источника
 * ker_ck блока без разрешения клока самого блока */
//#define CHIP_CLK_FMC_KER_SRC_CFG_EN         1
//#define CHIP_CLK_LPTIM1_KER_SRC_CFG_EN      1
//#define CHIP_CLK_LPTIM2_KER_SRC_CFG_EN      1
//#define CHIP_CLK_LPTIM3_KER_SRC_CFG_EN      1
//#define CHIP_CLK_LPTIM4_KER_SRC_CFG_EN      1
//#define CHIP_CLK_LPTIM5_KER_SRC_CFG_EN      1
#define CHIP_CLK_USART1_KER_SRC_CFG_EN   1
//#define CHIP_CLK_USART2_KER_SRC_CFG_EN      1
//#define CHIP_CLK_USART3_KER_SRC_CFG_EN      1
//#define CHIP_CLK_UART4_KER_SRC_CFG_EN       1
//#define CHIP_CLK_UART5_KER_SRC_CFG_EN       1
//#define CHIP_CLK_USART6_KER_SRC_CFG_EN      1
//#define CHIP_CLK_UART7_KER_SRC_CFG_EN       1
//#define CHIP_CLK_UART8_KER_SRC_CFG_EN       1
//#define CHIP_CLK_LPUART1_KER_SRC_CFG_EN     1
//#define CHIP_CLK_QSPI_KER_SRC_CFG_EN        1
//#define CHIP_CLK_SPI1_KER_SRC_CFG_EN        1
//#define CHIP_CLK_SPI2_KER_SRC_CFG_EN        1
#define CHIP_CLK_SPI3_KER_SRC_CFG_EN        1
//#define CHIP_CLK_SPI4_KER_SRC_CFG_EN        1
//#define CHIP_CLK_SPI5_KER_SRC_CFG_EN        1
//#define CHIP_CLK_SPI6_KER_SRC_CFG_EN        1
//#define CHIP_CLK_I2C1_KER_SRC_CFG_EN        1
//#define CHIP_CLK_I2C2_KER_SRC_CFG_EN        1
//#define CHIP_CLK_I2C3_KER_SRC_CFG_EN        1
//#define CHIP_CLK_I2C4_KER_SRC_CFG_EN        1
//#define CHIP_CLK_SDMMC1_KER_SRC_CFG_EN      1
//#define CHIP_CLK_SDMMC2_KER_SRC_CFG_EN      1
//#define CHIP_CLK_ADC1_KER_SRC_CFG_EN        1
//#define CHIP_CLK_ADC2_KER_SRC_CFG_EN        1
//#define CHIP_CLK_ADC3_KER_SRC_CFG_EN        1
//#define CHIP_CLK_USB1_OTGHS_KER_SRC_CFG_EN  1
//#define CHIP_CLK_USB2_OTGHS_KER_SRC_CFG_EN  1
//#define CHIP_CLK_SAI1_KER_SRC_CFG_EN        1
//#define CHIP_CLK_SAI2_KER_SRC_CFG_EN        1
//#define CHIP_CLK_SAI3_KER_SRC_CFG_EN        1
//#define CHIP_CLK_SAI4A_KER_SRC_CFG_EN       1
//#define CHIP_CLK_SAI4B_KER_SRC_CFG_EN       1
//#define CHIP_CLK_SPDIF_KER_SRC_CFG_EN       1
//#define CHIP_CLK_DFSDM1_KER_SRC_CFG_EN      1
//#define CHIP_CLK_FDCAN_KER_SRC_CFG_EN       1
//#define CHIP_CLK_SWP_KER_SRC_CFG_EN         1
//#define CHIP_CLK_RNG_KER_SRC_CFG_EN         1




/* *****************************************************************************
   Разрашение клоков для периферийных блоков.
   Настройка определяет, нужно ли включать клок для этого блока при начальной
   инициализации.

   Помимо основного клока от шины APB/AHB, блок может использовать kernel clock,
   источник которого должен быть определен соответствующим параметром, указанным
   в коментарии. Разрешение клока блока при инициализации автоамтически разрешает
   настройку источника kernel clock (при наличии).

   Нулевое значеие настройки означает, что клок будет запрещен при инициализации.
   При этом его можно включить уже во время работы через chip_per_clk_en(),
   однако в случае наличия у блока отдельного kernel clock, его источник должен
   быть в этом случае настроен при инициализации, для чего его настройка должна
   быть в этом случае явно разрешена через параметр ***_KER_SRC_CFG_EN (см. выше)

   Для таймеров (не LP) ker_ck всегда настраивается и определяется:
    - TIM2,3,4,5,6,7,12,13,14: делителем CHIP_CLK_TIMX_DIV относительно AHB1
    - TIM1,8,15,16,17: делителем CHIP_CLK_TIMY_DIV относительно AHB2.
    - HTRIM: CHIP_CLK_HRTIM_KER_SRC
 ******************************************************************************/
/* system */
#define CHIP_CLK_SYSCFG_EN              1   /* APB4 */
#define CHIP_CLK_HSEM_EN                0   /* AHB4 */
#define CHIP_CLK_RTC_APB_EN             0   /* APB4 */
/* ram */
#define CHIP_CLK_SRAM1_EN               1   /* AHB2 */
#define CHIP_CLK_SRAM2_EN               1   /* AHB2 */
#define CHIP_CLK_SRAM3_EN               1   /* AHB2 */
#define CHIP_CLK_BKPRAM_EN              0   /* AHB4 */
#define CHIP_CLK_FMC_EN                 0   /* AHB3, ker_ck: CHIP_CLK_FMC_KER_SRC */
/* gpio */
#define CHIP_CLK_GPIOA_EN               1   /* AHB4 */
#define CHIP_CLK_GPIOB_EN               1   /* AHB4 */
#define CHIP_CLK_GPIOC_EN               1   /* AHB4 */
#define CHIP_CLK_GPIOD_EN               1   /* AHB4 */
#define CHIP_CLK_GPIOE_EN               1   /* AHB4 */
#define CHIP_CLK_GPIOF_EN               1   /* AHB4 */
#define CHIP_CLK_GPIOG_EN               1   /* AHB4 */
#define CHIP_CLK_GPIOH_EN               1   /* AHB4 */
#define CHIP_CLK_GPIOI_EN               1   /* AHB4 */
#define CHIP_CLK_GPIOJ_EN               1   /* AHB4 */
#define CHIP_CLK_GPIOK_EN               1   /* AHB4 */
/* dma */
#define CHIP_CLK_MDMA_EN                0   /* AHB3 */
#define CHIP_CLK_DMA2D_EN               0   /* AHB3 */
#define CHIP_CLK_BDMA_EN                0   /* AHB4 */
#define CHIP_CLK_DMA1_EN                0   /* AHB1 */
#define CHIP_CLK_DMA2_EN                0   /* AHB1 */
/* timers */
#define CHIP_CLK_HRTIM_EN               0   /* APB2, ker_ck: CHIP_CLK_HRTIM_KER_SRC */
#define CHIP_CLK_TIM1_EN                0   /* APB2, ker_ck: APB2/CHIP_CLK_TIMY_DIV */
#define CHIP_CLK_TIM2_EN                0   /* APB1, ker_ck: APB1/CHIP_CLK_TIMX_DIV */
#define CHIP_CLK_TIM3_EN                0   /* APB1, ker_ck: APB1/CHIP_CLK_TIMX_DIV */
#define CHIP_CLK_TIM4_EN                0   /* APB1, ker_ck: APB1/CHIP_CLK_TIMX_DIV */
#define CHIP_CLK_TIM5_EN                0   /* APB1, ker_ck: APB1/CHIP_CLK_TIMX_DIV */
#define CHIP_CLK_TIM6_EN                0   /* APB1, ker_ck: APB1/CHIP_CLK_TIMX_DIV */
#define CHIP_CLK_TIM7_EN                0   /* APB1, ker_ck: APB1/CHIP_CLK_TIMX_DIV */
#define CHIP_CLK_TIM8_EN                0   /* APB2, ker_ck: APB2/CHIP_CLK_TIMY_DIV */
#define CHIP_CLK_TIM12_EN               0   /* APB1, ker_ck: APB1/CHIP_CLK_TIMX_DIV */
#define CHIP_CLK_TIM13_EN               0   /* APB1, ker_ck: APB1/CHIP_CLK_TIMX_DIV */
#define CHIP_CLK_TIM14_EN               0   /* APB1, ker_ck: APB1/CHIP_CLK_TIMX_DIV */
#define CHIP_CLK_TIM15_EN               0   /* APB2, ker_ck: APB2/CHIP_CLK_TIMY_DIV */
#define CHIP_CLK_TIM16_EN               0   /* APB2, ker_ck: APB2/CHIP_CLK_TIMY_DIV */
#define CHIP_CLK_TIM17_EN               0   /* APB2, ker_ck: APB2/CHIP_CLK_TIMY_DIV */
#define CHIP_CLK_LPTIM1_EN              0   /* APB1, ker_ck: CHIP_CLK_LPTIM1_KER_SRC */
#define CHIP_CLK_LPTIM2_EN              0   /* APB4, ker_ck: CHIP_CLK_LPTIM2_KER_SRC */
#define CHIP_CLK_LPTIM3_EN              0   /* APB4, ker_ck: CHIP_CLK_LPTIM345_KER_SRC */
#define CHIP_CLK_LPTIM4_EN              0   /* APB4, ker_ck: CHIP_CLK_LPTIM345_KER_SRC */
#define CHIP_CLK_LPTIM5_EN              0   /* APB4, ker_ck: CHIP_CLK_LPTIM345_KER_SRC */
/* uart */
#define CHIP_CLK_USART1_EN              0   /* APB2, ker_ck: CHIP_CLK_USART16_KER_SRC */
#define CHIP_CLK_USART2_EN              0   /* APB1, ker_ck: CHIP_CLK_USART234578_KER_SRC */
#define CHIP_CLK_USART3_EN              0   /* APB1, ker_ck: CHIP_CLK_USART234578_KER_SRC */
#define CHIP_CLK_UART4_EN               0   /* APB1, ker_ck: CHIP_CLK_USART234578_KER_SRC */
#define CHIP_CLK_UART5_EN               0   /* APB1, ker_ck: CHIP_CLK_USART234578_KER_SRC */
#define CHIP_CLK_USART6_EN              0   /* APB2, ker_ck: CHIP_CLK_USART16_KER_SRC */
#define CHIP_CLK_UART7_EN               0   /* APB1, ker_ck: CHIP_CLK_USART234578_KER_SRC */
#define CHIP_CLK_UART8_EN               0   /* APB1, ker_ck: CHIP_CLK_USART234578_KER_SRC */
#define CHIP_CLK_LPUART1_EN             0   /* APB4, ker_ck: CHIP_CLK_LPUART1_KER_SRC */
/* spi */
#define CHIP_CLK_QSPI_EN                0   /* AHB3, ker_ck: CHIP_CLK_QSPI_KER_SRC */
#define CHIP_CLK_SPI1_EN                0   /* APB2, ker_ck: CHIP_CLK_SPI123_KER_SRC */
#define CHIP_CLK_SPI2_EN                0   /* APB1, ker_ck: CHIP_CLK_SPI123_KER_SRC */
#define CHIP_CLK_SPI3_EN                0   /* APB1, ker_ck: CHIP_CLK_SPI123_KER_SRC */
#define CHIP_CLK_SPI4_EN                0   /* APB2, ker_ck: CHIP_CLK_SPI45_KER_SRC */
#define CHIP_CLK_SPI5_EN                0   /* APB2, ker_ck: CHIP_CLK_SPI45_KER_SRC */
#define CHIP_CLK_SPI6_EN                0   /* APB4, ker_ck: CHIP_CLK_SPI6_KER_SRC */
/* i2c */
#define CHIP_CLK_I2C1_EN                0   /* APB1, ker_ck: CHIP_CLK_I2C123_KER_SRC */
#define CHIP_CLK_I2C2_EN                0   /* APB1, ker_ck: CHIP_CLK_I2C123_KER_SRC */
#define CHIP_CLK_I2C3_EN                0   /* APB1, ker_ck: CHIP_CLK_I2C123_KER_SRC */
#define CHIP_CLK_I2C4_EN                0   /* APB4, ker_ck: CHIP_CLK_I2C4_KER_SRC */
/* sdmmc */
#define CHIP_CLK_SDMMC1_EN              0   /* AHB3, ker_ck: CHIP_CLK_SDMMC_KER_SRC */
#define CHIP_CLK_SDMMC2_EN              0   /* AHB2, ker_ck: CHIP_CLK_SDMMC_KER_SRC */
/* usb */
#define CHIP_CLK_USB1_OTGHS_EN          0   /* AHB1, ker_ck: CHIP_CLK_USB_KER_SRC */
#define CHIP_CLK_USB1_OTGHS_ULPI_EN     0   /* AHB1 */
#define CHIP_CLK_USB2_OTGHS_EN          0   /* AHB1, ker_ck: CHIP_CLK_USB_KER_SRC */
#define CHIP_CLK_USB2_OTGHS_ULPI_EN     0   /* AHB1 */
/* ethernet */
#define CHIP_CLK_ETH1_MAC_EN            1   /* AHB1 */
#define CHIP_CLK_ETH1_TX_EN             1   /* AHB1 */
#define CHIP_CLK_ETH1_RX_EN             1   /* AHB1 */
/* adc/dac */
#define CHIP_CLK_ADC1_EN                0   /* AHB1, ker_ck: CHIP_CLK_ADC_KER_SRC */
#define CHIP_CLK_ADC2_EN                0   /* AHB1, ker_ck: CHIP_CLK_ADC_KER_SRC */
#define CHIP_CLK_ADC3_EN                0   /* AHB4, ker_ck: CHIP_CLK_ADC_KER_SRC */
#define CHIP_CLK_DAC1_EN                0   /* APB1 */
#define CHIP_CLK_DAC2_EN                0   /* APB1 */
/* audio */
#define CHIP_CLK_SAI1_EN                0   /* APB2, ker_ck: CHIP_CLK_SAI1_KER_SRC */
#define CHIP_CLK_SAI2_EN                0   /* APB2, ker_ck: CHIP_CLK_SAI23_KER_SRC */
#define CHIP_CLK_SAI3_EN                0   /* APB2, ker_ck: CHIP_CLK_SAI23_KER_SRC */
#define CHIP_CLK_SAI4_EN                0   /* APB4, ker_ck: CHIP_CLK_SAI4A_KER_SRC, CHIP_CLK_SAI4B_KER_SRC */
#define CHIP_CLK_DFSDM1_EN              0   /* APB2, ker_ck: CHIP_CLK_DFSDM1_KER_SRC */
#define CHIP_CLK_SPDIF_EN               0   /* APB1, ker_ck: CHIP_CLK_SPDIF_KER_SRC */
/* others */
#define CHIP_CLK_FDCAN_EN               0   /* APB1, ker_ck: CHIP_CLK_FDCAN_KER_SRC */
#define CHIP_CLK_SWP_EN                 0   /* APB1, ker_ck: CHIP_CLK_SWP_KER_SRC */
#define CHIP_CLK_JPGDEC_EN              0   /* AHB3 */
#define CHIP_CLK_DCMI_EN                0   /* AHB2 */
#define CHIP_CLK_CRYPT_EN               0   /* AHB2 */
#define CHIP_CLK_HASH_EN                0   /* AHB2 */
#define CHIP_CLK_RNG_EN                 0   /* AHB2, ker_ck: CHIP_CLK_RNG_KER_SRC */
#define CHIP_CLK_CRC_EN                 0   /* AHB4 */
#define CHIP_CLK_LTDC_EN                0   /* APB3 */
#define CHIP_CLK_WWDG1_EN               0   /* APB3 */
#define CHIP_CLK_CEC_EN                 0   /* APB1 */
#define CHIP_CLK_CRS_EN                 0   /* APB1 */
#define CHIP_CLK_OPAMP_EN               0   /* APB1 */
#define CHIP_CLK_MDIOS_EN               0   /* APB1 */
#define CHIP_CLK_COMP12_EN              0   /* APB4 */
#define CHIP_CLK_VREF_EN                0   /* APB4 */





/* ---------------------------- Watchdog -------------------------------------*/
/* включать ли Watchdog (используется IWDG) */
#ifdef USE_LBOOT
    #define CHIP_WDT_INIT               0
#else
    #define CHIP_WDT_INIT               1
    /* Время сброса контроллера в мс, если WDT не будет сброшен  */
    #define CHIP_WDT_TIMEOUT_MS     10000
    /* Разрешение аппаратного режима WDT (включается всегда по сбросу,
     * иначе запускается программно при старте) */
    #define CHIP_WDT_HW_MODE_EN         0
    /* Разрешение работы WDT в Standby режиме процессора (иначе - останов) */
    #define CHIP_WDT_ON_STANDBY_EN      1
    /* Разрешение работы WDT в Stop режиме процессора (иначе - останов) */
    #define CHIP_WDT_ON_STOP_EN         1
#endif

/* ----------------------------- Flash ----------------------------------------*/
/* включение функций управление flash-памятью в сборку */
#define CHIP_FLASH_FUNC_EN            1
/* Максимальное кол-во параллельно изменяемых битов во flash-памяти (64, 32, 16, 8).
   Влияет на скорость записи/старания и максимальное пиковое потребление */
#define CHIP_FLASH_WR_PARALLEL_BITS   64


/* ----------------------------- ltimer ----------------------------------------*/
/* Разрешение инициализации ltimer и включение возможности использования его для
 * вычисления таймаутов */
#define CHIP_LTIMER_EN              1



#endif // CHIP_CONFIG_H

